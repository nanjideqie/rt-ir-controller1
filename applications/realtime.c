/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-12-07     hcszh       the first version
 */
#include "realtime.h"

//#include <rtthread.h>
//#include <rtdevice.h>
//#include "ab32vg1.h"
#include <board.h>

struct rt_event rtc_1s_event;


static void rtc_isr(int vector, void *param)
{
    rt_interrupt_enter();
    if(RTCCON & BIT(18))
    {

        RTCCPND |= BIT(18);
        //rt_kprintf("A");
        rt_event_send(&rtc_1s_event, 1);

    }
    rt_interrupt_leave();
}
int rtc_setup(rt_uint32_t hour, rt_uint32_t minute, rt_uint32_t second)
{
    int ret = 0;
    ret = set_time(hour, minute, second);
    if (ret != RT_EOK)
    {
        rt_kprintf("set RTC time failed\n");
        return ret;
    }
    ret = rt_event_init(&rtc_1s_event, "event", RT_IPC_FLAG_PRIO);
    if (ret != RT_EOK)
    {
        rt_kprintf("init event failed.\n");
        return -1;
    }
    rt_hw_interrupt_install(IRQ_RTC_VECTOR, rtc_isr, RT_NULL, "rtc_isr");

    return ret;
}
//static int rtc_sample(void)
//{
//    rt_err_t ret = RT_EOK;
//    time_t now;
//
//    /* 设置日期 */
//    ret = set_date(2021, 06, 02);
//    if (ret != RT_EOK)
//    {
//        rt_kprintf("set RTC date failed\n");
//        return ret;
//    }
//
//    /* 设置时间 */
//    ret = set_time(11, 15, 50);
//    if (ret != RT_EOK)
//    {
//        rt_kprintf("set RTC time failed\n");
//        return ret;
//    }
//
//    rt_uint8_t result = rt_event_init(&rtc_1s_event, "event", RT_IPC_FLAG_PRIO);
//        if (result != RT_EOK)
//        {
//            rt_kprintf("init event failed.\n");
//            return -1;
//        }
//
//
//    return ret;
//}
//
//void rtc_irq(void)
//{
//    rt_hw_interrupt_install(IRQ_RTC_VECTOR, rtc_isr, RT_NULL, "rtc_isr");
//
//}
///* 在 APP 级自动执行该函数 */
//INIT_APP_EXPORT(rtc_sample);
//MSH_CMD_EXPORT(rtc_irq, rtc_irq);



