/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-11-21     hcszh       the first version
 */
#ifndef API_SDADC_H__
#define API_SDADC_H__

/**
 * @defgroup SDADC_Channel
 * @{
 */
//left sdadc channel config
#define CH_AUXL_PA6         0x01    //AUXL0(PA6) -> left aux  -> sdadc left channel
#define CH_AUXL_PB1         0x02    //AUXL1(PB1) -> left aux  -> sdadc left channel
#define CH_AUXL_PE6         0x03    //AUXL2(PE6) -> left aux  -> sdadc left channel
#define CH_AUXL_PF0         0x04    //AUXL3(PF0) -> left aux  -> sdadc left channel
#define CH_AUXL_VOUTLN      0x05    //VOUTLN     -> left aux  -> sdadc left channel
#define CH_AUXL_MICL        0x06    //MICL(PF2)  -> left aux  -> sdadc left channel
#define CH_AUXL_MICR        0x07    //MICR       -> left aux  -> sdadc left channel
#define CH_AUXL_VOUTRP      0x08    //VOUTRP     -> left aux  -> sdadc left channel
#define CH_MICL0            0x0c    //MICL(PF2)  -> left mic  -> sdadc left channel
#define CH_MICL1            0x0d    //MICR       -> left mic  -> sdadc left channel

//right sdadc channel config
#define CH_AUXR_PA7         0x10    //AUXR0(PA7) -> right aux -> sdadc right channel
#define CH_AUXR_PB2         0x20    //AUXR1(PB2) -> right aux -> sdadc right channel
#define CH_AUXR_PE7         0x30    //AUXR2(PE7) -> right aux -> sdadc right channel
#define CH_AUXR_PF1         0x40    //AUXR3(PF1) -> right aux -> sdadc right channel
#define CH_AUXR_VOUTLN      0x50    //VOUTLN     -> right aux -> sdadc right channel
#define CH_AUXR_MICL        0x60    //MICL(PF2)  -> right aux -> sdadc right channel
#define CH_AUXR_MICR        0x70    //MICR       -> right aux -> sdadc right channel
#define CH_AUXR_VOUTRN      0x80    //VOUTRN     -> right aux -> sdadc right channel
#define CH_MICR0            0xc0    //MICR       -> right mic -> sdadc right channel
#define CH_MICR1            0xd0    //MICL(PF2)  -> right mic -> sdadc right channel
/**
 * @}
 *
 */

#define CHANNEL_L   0x0F
#define CHANNEL_R   0xF0

/**
 * @defgroup SDADC_Message
 * @{
 */
enum sdadc_msg {
    MSG_SDADC_LHALF_DONE = 1,
    MSG_SDADC_LALL_DONE,
    MSG_SDADC_RHALF_DONE,
    MSG_SDADC_RALL_DONE,
};
/**
 * @}
 *
 */

/**
 * @brief   Set the SDADC channel.
 *
 * @param   channel This parameter can be a value of @ref SDADC_Channel
 * @return  int
 *          0: OK
 *         -1: ERROR
 */
int sdadc_set_channel(rt_uint8_t channel);

/**
 * @brief   Set the SDADC sample rate.
 *
 * @param   sample_rate support 48000, 44100, 38000, 32000, 24000, 22050, 16000, 12000, 11025, 8000.
 * @return  int
 *          0: OK
 *         -1: ERROR
 */
int sdadc_set_sample_rate(rt_uint16_t sample_rate);

/**
 * @brief   Set the SDADC gain.
 *
 * @param   gain Analog gain will only be set before the SDADC works.
 *          [16:5] analog gain. Range from 0 to 23. Step is 3DB.(-6db ~ +63db)
 *          [ 4:0] digital gain. Range from 0 to 31. Step is 3/32 DB.(0db ~ +3db)
 * @return  int
 *          0: OK
 *         -1: ERROR
 */
int sdadc_set_gain(rt_uint16_t gain);

/**
 * @brief   Set the SDADC DMA samples.
 *
 * @param   samples
 * @return  int
 *          0: OK
 *         -1: ERROR
 */
int sdadc_set_dma_samples(rt_uint16_t samples);

/**
 * @brief   Get the SDADC channel.
 *
 * @return  int channel
 */
int sdadc_get_channel(void);

/**
 * @brief   Get the SDADC sample rate.
 *
 * @return  int sample_rate
 */
int sdadc_get_sample_rate(void);

/**
 * @brief   Get the SDADC gain.
 *
 * @return  int gain
 */
int sdadc_get_gain(void);

/**
 * @brief   Get the SDADC DMA samples.
 *
 * @return  int samples
 */
int sdadc_get_dma_samples(void);

/**
 * @brief   Start the SDADC.
 *
 * @param   rx_buf The SDADC DMA buffer address.
 * @return  int
 *          0: OK
 *         -1: ERROR
 */
int sdadc_start(void *rx_buf);

/**
 * @brief   Close the SDADC.
 *
 * @return  int
 *          0: OK
 *         -1: ERROR
 */
int sdadc_exit(void);

/**
 * @brief   Will be called when SDADC DMA done.
 *          It needs to be in RAM and reimplemented.
 *
 * @param   msg This parameter can be a value of @ref SDADC_Message
 */
void sdadc_dma_notice(enum sdadc_msg msg);

/**
 * @brief   The SDADC DMA interrupt. Need to be registered.
 *          It needs to be in RAM and registered.
 *
 */
void sdadc_isr(void);

#endif


