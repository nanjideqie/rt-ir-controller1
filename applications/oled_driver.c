/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-12-06     hcszh       the first version
 */
#include "oled_driver.h"
#include "board.h"
#include "oledfont.h"

 rt_base_t oled_scl_pin;
 rt_base_t oled_sda_pin;
 rt_base_t oled_rst_pin;;
 rt_base_t oled_dc_pin;

#define OLED_SCL_CLR() rt_pin_write(oled_scl_pin, PIN_LOW)
#define OLED_SCL_SET() rt_pin_write(oled_scl_pin, PIN_HIGH)

#define OLED_SDA_CLR() rt_pin_write(oled_sda_pin, PIN_LOW)
#define OLED_SDA_SET() rt_pin_write(oled_sda_pin, PIN_HIGH)

#define OLED_RST_CLR() rt_pin_write(oled_rst_pin, PIN_LOW)
#define OLED_RST_SET() rt_pin_write(oled_rst_pin, PIN_HIGH)

#define OLED_DC_CLR() rt_pin_write(oled_dc_pin, PIN_LOW)
#define OLED_DC_SET() rt_pin_write(oled_dc_pin, PIN_HIGH)

#define OLED_CS_CLR() {}
#define OLED_CS_SET() {}

#define OLED_CMD  0  //写命令
#define OLED_DATA 1 //写数据

 //写入一个字节
 void OLED_WR_Byte(rt_uint8_t dat,rt_uint8_t cmd)
 {
   rt_uint8_t i;
   if(cmd)
     OLED_DC_SET();
   else
     OLED_DC_CLR();
   OLED_CS_CLR();

   for(i=0;i<8;i++)
       {
        OLED_SCL_CLR();
           if(dat&0x80)
           {
            OLED_SDA_SET();
           }
           else
           {
            OLED_SDA_CLR();
           }
         OLED_SCL_SET();
           dat<<=1;
       }


   OLED_CS_SET();
   OLED_DC_SET();
 }

void oled_gpio_init()
{
     oled_scl_pin = rt_pin_get(OLED_SCL_PIN);
      oled_sda_pin = rt_pin_get(OLED_SDA_PIN);
      oled_rst_pin = rt_pin_get(OLED_RST_PIN);//板子丝印画错了
      oled_dc_pin = rt_pin_get(OLED_DC_PIN);
    rt_pin_mode(oled_scl_pin, PIN_MODE_OUTPUT);
    rt_pin_mode(oled_sda_pin, PIN_MODE_OUTPUT);
    rt_pin_mode(oled_rst_pin, PIN_MODE_OUTPUT);
    rt_pin_mode(oled_dc_pin, PIN_MODE_OUTPUT);
}




//反显函数
void OLED_ColorTurn(rt_uint8_t i)
{
  if(!i) OLED_WR_Byte(0xA6,OLED_CMD);//正常显示
  else  OLED_WR_Byte(0xA7,OLED_CMD);//反色显示
}

//屏幕旋转180度
void OLED_DisplayTurn(rt_uint8_t i)
{
  if(i==0)
    {
      OLED_WR_Byte(0xC8,OLED_CMD);//正常显示
      OLED_WR_Byte(0xA1,OLED_CMD);
    }
else
    {
      OLED_WR_Byte(0xC0,OLED_CMD);//反转显示
      OLED_WR_Byte(0xA0,OLED_CMD);
    }
}


//坐标设置

void OLED_SET_Pos(rt_uint8_t x, rt_uint8_t y)
{
  OLED_WR_Byte(0xb0+y,OLED_CMD);
  OLED_WR_Byte(((x&0xf0)>>4)|0x10,OLED_CMD);
  OLED_WR_Byte((x&0x0f),OLED_CMD);
}
//开启OLED显示
void OLED_Display_On(void)
{
  OLED_WR_Byte(0X8D,OLED_CMD);  //SET DCDC命令
  OLED_WR_Byte(0X14,OLED_CMD);  //DCDC ON
  OLED_WR_Byte(0XAF,OLED_CMD);  //DISPLAY ON
}
//关闭OLED显示
void OLED_Display_Off(void)
{
  OLED_WR_Byte(0X8D,OLED_CMD);  //SET DCDC命令
  OLED_WR_Byte(0X10,OLED_CMD);  //DCDC OFF
  OLED_WR_Byte(0XAE,OLED_CMD);  //DISPLAY OFF
}
//清屏函数,清完屏,整个屏幕是黑色的!和没点亮一样!!!
void OLED_Clear(void)
{
  rt_uint8_t i,n;
  for(i=0;i<8;i++)
  {
    OLED_WR_Byte (0xb0+i,OLED_CMD);    //设置页地址（0~7）
    OLED_WR_Byte (0x00,OLED_CMD);      //设置显示位置—列低地址
    OLED_WR_Byte (0x10,OLED_CMD);      //设置显示位置—列高地址
    for(n=0;n<128;n++)OLED_WR_Byte(0,OLED_DATA);
  } //更新显示
}


//在指定位置显示一个字符
//x:0~127
//y:0~63
//sizey:选择字体 6x8  8x16
void OLED_ShowChar(rt_uint8_t x,rt_uint8_t y,const rt_uint8_t chr,rt_uint8_t sizey)
{
  rt_uint8_t c=0,sizex=sizey/2,temp;
  rt_uint16_t i=0,size1;
  if(sizey==8)size1=6;
  else size1=(sizey/8+((sizey%8)?1:0))*(sizey/2);
  c=chr-' ';//得到偏移后的值
  OLED_SET_Pos(x,y);
  for(i=0;i<size1;i++)
  {
    if(i%sizex==0&&sizey!=8) OLED_SET_Pos(x,y++);
    if(sizey==8)
    {
      temp=asc2_0806[c][i];
      OLED_WR_Byte(temp,OLED_DATA);//6X8字号
    }
    else if(sizey==16)
    {
      temp=asc2_1608[c][i];
      OLED_WR_Byte(temp,OLED_DATA);//8x16字号
    }
    else return;
  }
}
//m^n函数
rt_uint32_t oled_pow(rt_uint8_t m,rt_uint8_t n)
{
  rt_uint32_t result=1;
  while(n--)result*=m;
  return result;
}
//显示数字
//x,y :起点坐标
//num:要显示的数字
//len :数字的位数
//sizey:字体大小
void OLED_ShowNum(rt_uint8_t x,rt_uint8_t y,rt_uint32_t num,rt_uint8_t len,rt_uint8_t sizey)
{
  rt_uint8_t t,temp,m=0;
  rt_uint8_t enshow=0;
  if(sizey==8)m=2;
  for(t=0;t<len;t++)
  {
    temp=(num/oled_pow(10,len-t-1))%10;
    if(enshow==0&&t<(len-1))
    {
      if(temp==0)
      {
        OLED_ShowChar(x+(sizey/2+m)*t,y,' ',sizey);
        continue;
      }else enshow=1;
    }
    OLED_ShowChar(x+(sizey/2+m)*t,y,temp+'0',sizey);
  }
}
//显示一个字符号串
void OLED_ShowString(rt_uint8_t x,rt_uint8_t y,const char *chr,rt_uint8_t sizey)
{
  rt_uint8_t j=0;
  rt_uint8_t x0 = x;
  while (chr[j]!='\0' && ((x0>>3)+j)<16)
  {
    OLED_ShowChar(x,y,chr[j++],sizey);
    if(sizey==8)x+=6;
    else x+=sizey/2;
  }
}
//显示汉字
void OLED_ShowChinese(rt_uint8_t x,rt_uint8_t y,const rt_uint8_t no,rt_uint8_t sizey)
{
  rt_uint16_t i,size1=(sizey/8+((sizey%8)?1:0))*sizey;
  rt_uint8_t temp;
  for(i=0;i<size1;i++)
  {
    if(i%sizey==0) OLED_SET_Pos(x,y++);
    if(sizey==16)
    {
      temp=Hzk[no][i];
      OLED_WR_Byte(temp,OLED_DATA);//16x16字号
    }
//    else if(sizey==xx) OLED_WR_Byte(xxx[c][i],OLED_DATA);//用户添加字号
    else return;
  }
}


//显示图片
//x,y显示坐标
//sizex,sizey,图片长宽
//BMP：要显示的图片
void OLED_DrawBMP(rt_uint8_t x,rt_uint8_t y,rt_uint8_t sizex, rt_uint8_t sizey,const rt_uint8_t BMP[])
{
  rt_uint16_t j=0;
  rt_uint8_t i,m,temp;
  sizey=sizey/8+((sizey%8)?1:0);
  for(i=0;i<sizey;i++)
  {
    OLED_SET_Pos(x,i+y);
    for(m=0;m<sizex;m++)
    {
       temp=BMP[j++];
       OLED_WR_Byte(temp,OLED_DATA);
    }
  }
} //OLED的初始化
void OLED_Init(void)
{
  oled_gpio_init();
  OLED_RST_CLR();
  rt_thread_mdelay(200);
  OLED_RST_SET();

  OLED_WR_Byte(0xAE,OLED_CMD);//--turn off oled panel
  OLED_WR_Byte(0x00,OLED_CMD);//---set low column address
  OLED_WR_Byte(0x10,OLED_CMD);//---set high column address
  OLED_WR_Byte(0x40,OLED_CMD);//--set start line address  SET Mapping RAM Display Start Line (0x00~0x3F)
  OLED_WR_Byte(0x81,OLED_CMD);//--set contrast control register
  OLED_WR_Byte(0xCF,OLED_CMD); // SET SEG Output Current Brightness
  OLED_WR_Byte(0xA1,OLED_CMD);//--SET SEG/Column Mapping     0xa0左右反置 0xa1正常
  OLED_WR_Byte(0xC8,OLED_CMD);//SET COM/Row Scan Direction   0xc0上下反置 0xc8正常
  OLED_WR_Byte(0xA6,OLED_CMD);//--set normal display
  OLED_WR_Byte(0xA8,OLED_CMD);//--set multiplex ratio(1 to 64)
  OLED_WR_Byte(0x3f,OLED_CMD);//--1/64 duty
  OLED_WR_Byte(0xD3,OLED_CMD);//-set display offset Shift Mapping RAM Counter (0x00~0x3F)
  OLED_WR_Byte(0x00,OLED_CMD);//-not offset
  OLED_WR_Byte(0xd5,OLED_CMD);//--set display clock divide ratio/oscillator frequency
  OLED_WR_Byte(0x80,OLED_CMD);//--set divide ratio, SET Clock as 100 Frames/Sec
  OLED_WR_Byte(0xD9,OLED_CMD);//--set pre-charge period
  OLED_WR_Byte(0xF1,OLED_CMD);//SET Pre-Charge as 15 Clocks & Discharge as 1 Clock
  OLED_WR_Byte(0xDA,OLED_CMD);//--set com pins hardware configuration
  OLED_WR_Byte(0x12,OLED_CMD);
  OLED_WR_Byte(0xDB,OLED_CMD);//--set vcomh
  OLED_WR_Byte(0x40,OLED_CMD);//SET VCOM Deselect Level
  OLED_WR_Byte(0x20,OLED_CMD);//-SET Page Addressing Mode (0x00/0x01/0x02)
  OLED_WR_Byte(0x02,OLED_CMD);//
  OLED_WR_Byte(0x8D,OLED_CMD);//--set Charge Pump enable/disable
  OLED_WR_Byte(0x14,OLED_CMD);//--set(0x10) disable
  OLED_WR_Byte(0xA4,OLED_CMD);// Disable Entire Display On (0xa4/0xa5)
  OLED_WR_Byte(0xA6,OLED_CMD);// Disable Inverse Display On (0xa6/a7)
  OLED_Clear();
  OLED_WR_Byte(0xAF,OLED_CMD); /*display ON*/
}

int oled_setup(void)
{
  OLED_Init();
  OLED_ColorTurn(0);//0正常显示 1反色显示
  OLED_DisplayTurn(0);//0正常显示 1翻转180度显示
  return 0;
}

//INIT_APP_EXPORT(oled_setup);
