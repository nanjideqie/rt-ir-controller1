/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-11-21     hcszh       the first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include "board.h"
#include <multi_button.h>
#include "interrupt.h"
#include "irrx_app.h"
//按键获取
#define BUTTON_PIN_0 rt_pin_get("PF.0")
#define BUTTON_PIN_1 rt_pin_get("PF.1")
extern rt_uint8_t main_mode;
//按键结构体
static struct button btn_0;
static struct button btn_1;

static uint32_t cnt_0 = 0;
static uint32_t cnt_1 = 0;

//读取按键驱动
static uint8_t button_read_pin_0(void)
{
    return rt_pin_read(BUTTON_PIN_0);
}

static uint8_t button_read_pin_1(void)
{
    return rt_pin_read(BUTTON_PIN_1);
}


static void button_0_callback(void *btn)
{
    uint32_t btn_event_val;

    btn_event_val = get_button_event((struct button *)btn);

    switch(btn_event_val)
    {
    case SINGLE_CLICK:
        tim1_sample();
        rt_kprintf("button 0 single click\n");
    break;

    case DOUBLE_CLICK:
        rt_kprintf("button 0 double click\n");
    break;

    case LONG_PRESS_START:
        rt_kprintf("button 0 long press start\n");
    break;

    case LONG_PRESS_HOLD:
        rt_kprintf("button 0 long press hold\n");
    break;
    }
}
//按键1回调函数
static void button_1_callback(void *btn)
{
    uint32_t btn_event_val;

    btn_event_val = get_button_event((struct button *)btn);

    switch(btn_event_val)
    {
    case SINGLE_CLICK:
        if(main_mode == 0)
        {
            rt_hw_irq_enable(IRQ_IRRX_VECTOR);
            main_mode = 1;
        }
        else {
            rt_hw_irq_disable(IRQ_IRRX_VECTOR);
                        main_mode = 0;
        }
        rt_kprintf("mode: %d\n", main_mode);

    break;

    case DOUBLE_CLICK:
        rt_kprintf("button 1 double click\n");
    break;

    case LONG_PRESS_START:
        rt_kprintf("button 1 long press start\n");
    break;

    case LONG_PRESS_HOLD:
        rt_kprintf("button 1 long press hold\n");
    break;
    }
}


//按键任务实体
static void btn_thread_entry(void* p)
{
    while(1)
    {
        /* 5ms */
        rt_thread_delay(RT_TICK_PER_SECOND/200);
        button_ticks();
    }
}
//按键初始化
static int multi_button_test(void)
{
    rt_thread_t thread = RT_NULL;

    /* Create background ticks thread */
    thread = rt_thread_create("btn", btn_thread_entry, RT_NULL, 512, 10, 10);
    if(thread == RT_NULL)
    {
        return RT_ERROR;
    }
    rt_thread_startup(thread);

    /* low level drive */
    rt_pin_mode  (BUTTON_PIN_0, PIN_MODE_INPUT_PULLUP);
    button_init  (&btn_0, button_read_pin_0, PIN_LOW);
    button_attach(&btn_0, SINGLE_CLICK,     button_0_callback);
    button_attach(&btn_0, DOUBLE_CLICK,     button_0_callback);
    button_attach(&btn_0, LONG_PRESS_START, button_0_callback);
    button_attach(&btn_0, LONG_PRESS_HOLD,  button_0_callback);
    button_start (&btn_0);

    rt_pin_mode  (BUTTON_PIN_1, PIN_MODE_INPUT_PULLUP);
    button_init  (&btn_1, button_read_pin_1, PIN_LOW);
    button_attach(&btn_1, SINGLE_CLICK,     button_1_callback);
    button_attach(&btn_1, DOUBLE_CLICK,     button_1_callback);
    button_attach(&btn_1, LONG_PRESS_START, button_1_callback);
    button_attach(&btn_1, LONG_PRESS_HOLD,  button_1_callback);
    button_start (&btn_1);

    return RT_EOK;
}
//添加到系统初始化
INIT_APP_EXPORT(multi_button_test);
