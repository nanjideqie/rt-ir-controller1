/*
 * Copyright (c) 2020-2021, Bluetrum Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020/12/10     greedyhao    The first version
 */

/**
 * Notice!
 * All functions or data that are called during an interrupt need to be in RAM.
 * You can do it the way exception_isr() does.
 */

#include <rtthread.h>
#include "board.h"
//#include "dfs_posix.h"
#include "oled_driver.h"
#include "realtime.h"
#include "dhtxx.h"
#include "irrx_app.h"
#define THREAD_PRIORITY         25
#define THREAD_STACK_SIZE       512
#define THREAD_TIMESLICE        5
#define DTH11_PIN 5

rt_uint32_t hh = 0, mm = 0, ss = 0;

static char mb_pool[16];
static rt_thread_t tid1 = RT_NULL;
static rt_thread_t tid_update_oled = RT_NULL;
static void thread1_entry(void *parameter);
static void update_oled_entry(void *parameter);
static int oled_init(void *parameter);
static int rtc_init(void *parameter);
static int dh11_init(void *parameter);

rt_uint8_t ac_state = 0;
rt_uint8_t* ac_state_text[3] = { "off ", "hot ", "cool" };
dht_device_t dht11_sensor = RT_NULL;
extern struct rt_event rtc_1s_event;
static rt_uint32_t temp_high = 240;
static rt_uint32_t temp_low = 200;
static rt_uint8_t code_cnt = 0;
//oled text
char oled_txt1[16] = "";
char oled_txt2[16] = "";
char oled_txt3[16] = "";
char oled_txt4[16] = "";
rt_int32_t temp = 0, humi = 0;
rt_uint8_t main_mode = 0; // normal mode

//static void thread_record_entry(void* parameter);
struct rt_mailbox mb;
extern int tim1_sample(void);
int main(void)
{
    time_t now_m;
    rt_err_t result;

    /* 初始化一个 mailbox */
    result = rt_mb_init(&mb, "mbt", /* 名称是 mbt */
    &mb_pool[0], /* 邮箱用到的内存池是 mb_pool */
    sizeof(mb_pool) / 4, /* 邮箱中的邮件数目，因为一封邮件占 4 字节 */
    RT_IPC_FLAG_FIFO); /* 采用 FIFO 方式进行线程等待 */
    if (result != RT_EOK)
    {
        rt_kprintf("init mailbox failed.\n");
        return -1;
    }

    //init device
    rtc_init(RT_NULL);
    oled_init(RT_NULL);
    dh11_init(RT_NULL);
    tid1 = rt_thread_create("thread1", thread1_entry, RT_NULL,
    THREAD_STACK_SIZE,
    THREAD_PRIORITY, THREAD_TIMESLICE);
    if (tid1 != RT_NULL)
        rt_thread_startup(tid1);
    tid_update_oled = rt_thread_create("thread_update_oled", update_oled_entry, RT_NULL,
    THREAD_STACK_SIZE,
    THREAD_PRIORITY, THREAD_TIMESLICE);
    if (tid_update_oled != RT_NULL)
        rt_thread_startup(tid_update_oled);

    while (1)
    {
        now_m = time(RT_NULL);
        if (dht_read(dht11_sensor))
        {

            temp = dht_get_temperature(dht11_sensor);
            humi = dht_get_humidity(dht11_sensor);
            if (temp > temp_high && ac_state != 2)
            {
                //制冷
                tim1_sample();
                ac_state = 2;
            }
            else if (temp < temp_low && ac_state != 1)
            {
                //制热
                tim1_sample();
                ac_state = 1;
            }
//            else
//            {
//                //关机
//                if (ac_state != 0)
//                    tim1_sample();
//                ac_state = 0;
//            }
        }

        if (rt_strncmp("15:10", ctime(&now_m) + 11, rt_strlen("15:10")) == 0 && ac_state != 1)
        {
            tim1_sample();
            ac_state = 1;
            rt_kprintf(ctime(&now_m));
        }
        rt_thread_mdelay(1000);

    }
}

static void thread1_entry(void *parameter)
{
    rt_uint32_t count = 0;
    rt_uint32_t *irrx_data;
    while (1)
    {
        if (rt_mb_recv(&mb, (rt_ubase_t *) &irrx_data, RT_WAITING_FOREVER) == RT_EOK)
        {
            rt_kprintf("code %d :\n", code_cnt++);
            for (int i = 0; i < irrx_data[0]; i++)
                rt_kprintf("%d ", irrx_data[i + 1]);
//            rt_kprintf("addr=%hx cmd=%hx\n", irrx_data[0], irrx_data[1]);
        }
        rt_thread_mdelay(100);

    }
    rt_mb_detach(&mb);
}

//显示线程

static void update_oled_entry(void *parameter)
{
    rt_uint32_t e;
    time_t now;
    char tempstr[16] = "";
    while (1)
    {
        if (rt_event_recv(&rtc_1s_event, 1,
        RT_EVENT_FLAG_OR | RT_EVENT_FLAG_CLEAR,
        RT_WAITING_FOREVER, &e) == RT_EOK)
        {
            if (main_mode == 0)
            {
                now = time(RT_NULL);
                rt_memcpy(tempstr, ctime(&now) + 11, 8);
                tempstr[9] = '\0';
                rt_memcpy(oled_txt2, tempstr, rt_strlen(tempstr) + 1);
                rt_memcpy(oled_txt1, "RT-IR controller", rt_strlen("RT-IR controller") + 1);
                rt_sprintf(oled_txt3, "%dC %d%  ", temp / 10, humi / 10);
                rt_memcpy(oled_txt4, ac_state_text[ac_state], rt_strlen(ac_state_text[ac_state]) + 1);
            }
            if (main_mode == 1)
            {
                rt_memcpy(oled_txt1, "Learning...     ", rt_strlen("Learning...     ") + 1);
                rt_sprintf(oled_txt2, "CODE%d          ", code_cnt);
                rt_memcpy(oled_txt3, "                ", rt_strlen("                ") + 1);
                rt_memcpy(oled_txt4, "                ", rt_strlen("                ") + 1);
            }
            OLED_ShowString(0, 0, oled_txt1, 16);
            OLED_ShowString(0, 2, oled_txt2, 16);
            OLED_ShowString(0, 4, oled_txt3, 16);
            OLED_ShowString(0, 6, oled_txt4, 16);
        }

        rt_thread_mdelay(100);
    }
}

static int oled_init(void *parameter)
{
    ac_state = 0;
    oled_setup();
    rt_memcpy(oled_txt1, "RT-IR controller", 16);
    rt_memcpy(oled_txt3, "0.0C 0.0%", rt_strlen("0.0C 0.0%") + 1);
    rt_memcpy(oled_txt4, ac_state_text[ac_state], rt_strlen(ac_state_text[ac_state]) + 1);
    OLED_ShowString(0, 0, oled_txt1, 16);
    rt_kprintf("oled OK!");
    return 0;
}

static int rtc_init(void *parameter)
{
    int ret = 0;
    hh = 15;
    mm = 15;
    ss = 33;
    ret = rtc_setup(hh, mm, ss);
    rt_kprintf("rtc OK!\n");
    return ret;
}
static int dh11_init(void *parameter)
{
    int ret = 0;

    dht11_sensor = dht_create(DTH11_PIN);
    rt_kprintf("dht11 OK!\n");
}
