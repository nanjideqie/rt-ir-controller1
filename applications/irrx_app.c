/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-12-11     hcszh       the first version
 */
#include "board.h"
#include <rtthread.h>
#include <rtdevice.h>
#include <irrx_app.h>
#include "pwm.h"
#define HWTIMER_DEV_NAME   "timer1"     /* 定时器名称 */

uint8_t irrxpin_isHigh = 0;
uint8_t irrx_isLow = 1;
rt_uint32_t i = 0;
#define PWM_DEV_CHANNEL     1       /* PWM通道 */

extern struct rt_device_pwm *pwm_dev;

static void toggle_pin(void);

void toggle_pwm(void)
{
    if (irrx_isLow)
    {
        //enable tim5 pwm2
        rt_pwm_enable(pwm_dev, PWM_DEV_CHANNEL);
        irrx_isLow = 0;
    }
    else
    {
        //disable tim5pwm2
        rt_pwm_disable(pwm_dev, PWM_DEV_CHANNEL);
        irrx_isLow = 1;
    }
}
//rt_uint32_t send_data[16] = {1000000, 4546, 4355, 635, 1533, 634, 444, 635, 1530, 637, 1533, 634, 442, 635, 443, 0};
rt_uint32_t send_data[256] = {2000000, 4546, 4355, 635, 1533, 634, 444, 635, 1530, 637, 1533, 634, 442, 635, 443, 634,
        1557, 611, 467, 611, 467, 610, 1558, 609, 470, 607, 471, 606, 1563, 604, 1591, 576, 502, 576, 1592, 576, 1591,
        577, 501, 577, 1592, 576, 1591, 576, 1592, 577, 1591, 576, 1592, 576, 1592, 576, 501, 577, 1592, 575, 501, 576,
        501, 576, 502, 576, 502, 576, 502, 577, 501, 576, 1592, 576, 502, 576, 502, 576, 501, 576, 1592, 576, 1591, 576,
        502, 576, 502, 576, 500, 576, 1592, 576, 1592, 576, 1592, 576, 502, 574, 503, 575, 1592, 576, 1591, 576, 5219,
        4485, 4420, 575, 1592, 575, 503, 575, 1593, 576, 1592, 575, 503, 575, 503, 575, 1593, 575, 503, 575, 503, 574,
        1593, 575, 502, 576, 503, 575, 1593, 574, 1594, 574, 504, 574, 1593, 574, 1594, 575, 502, 575, 1593, 575, 1593,
        574, 1594, 574, 1594, 574, 1594, 574, 1594, 573, 505, 574, 1593, 572, 504, 575, 503, 574, 503, 574, 505, 572,
        504, 574, 504, 574, 1595, 573, 503, 574, 504, 574, 504, 573, 1594, 573, 1595, 573, 505, 573, 505, 573, 505, 573,
        1595, 572, 1620, 548, 1620, 548, 530, 548, 530, 548, 1596, 572, 1620, 548, 0 };

//rt_uint16_t *send_data = send_hot;
static void tim1_isr(int vector, void *param)
{
    rt_interrupt_enter();

    if (TMR1CON & BIT(9))
    {
        TMR1CON ^= BIT(0);
        TMR1CPND |= BIT(9);
        if (send_data[i] > 1)
        {
            TMR1PR = send_data[i] - 1;
            //rt_kprintf("%d\n", send_data[i]);
            i++;
            toggle_pwm();
            TMR1CON |= BIT(0);
        }
        else
        {
            rt_pwm_disable(pwm_dev, PWM_DEV_CHANNEL);
            irrx_isLow = 1;
            i = 0;
        }

    }

    rt_interrupt_leave();
}
static void tim1_init(void)
{

    Pwm_Init();
    i = 0;
    TMR1CNT = 0;
    TMR1PR = send_data[i++] - 1;                                //1000ms Timer overflow interrupt
    TMR1CON = BIT(7) | BIT(2) | BIT(0); //capture & overflow interrupt enable, falling edge, Capture Mode
    rt_hw_interrupt_install(IRQ_TMR1_VECTOR, tim1_isr, RT_NULL, "tim1_isr");

}

int tim1_sample(void)
{
//    rt_thread_t thread = RT_NULL;
//
//       /* Create background ticks thread */
//       thread = rt_thread_create("tim1", tim1_thread_entry, RT_NULL, 256, 10, 10);
//       rt_kprintf("tim1ok\n");
//       if(thread == RT_NULL)
//           {
//               return RT_ERROR;
//           }
//           rt_thread_startup(thread);
    tim1_init();
}

/* 定时器超时回调函数 */
//static rt_err_t timeout_cb(rt_device_t dev, rt_size_t size)
//{
//    //rt_kprintf("this is hwtimer timeout callback fucntion!\n");
//    //rt_kprintf("tick is :%d !\n", rt_tick_get());
//    toggle_pin();
//    return 0;
//}
//
//static int hwtimer_sample(int argc, char *argv[])
//{
//    rt_err_t ret = RT_EOK;
//
//    rt_device_t hw_dev = RT_NULL; /* 定时器设备句柄 */
//    rt_hwtimer_mode_t mode; /* 定时器模式 */
//    rt_uint32_t freq = 1000000; /* 计数频率 */
//    rt_hwtimerval_t timeout_s; /* 定时器超时值 */
//
//    /* 查找定时器设备 */
//    hw_dev = rt_device_find(HWTIMER_DEV_NAME);
//    if (hw_dev == RT_NULL)
//    {
//        rt_kprintf("hwtimer sample run failed! can't find %s device!\n", HWTIMER_DEV_NAME);
//        return RT_ERROR;
//    }
//
//    /* 以读写方式打开设备 */
//    ret = rt_device_open(hw_dev, RT_DEVICE_OFLAG_WRONLY);
//    if (ret != RT_EOK)
//    {
//        rt_kprintf("open %s device failed!\n", HWTIMER_DEV_NAME);
//        return ret;
//    }
//
//    /* 设置超时回调函数 */
//    rt_device_set_rx_indicate(hw_dev, timeout_cb);
//
//    /* 设置计数频率(默认1Mhz或支持的最小计数频率) */
//    ret = rt_device_control(hw_dev, HWTIMER_CTRL_FREQ_SET, &freq);
//    if (ret != RT_EOK)
//    {
//        rt_kprintf("set frequency failed! ret is :%d\n", ret);
//        return ret;
//    }
//
//    /* 设置模式为周期性定时器 */
//    mode = HWTIMER_MODE_PERIOD;
//    ret = rt_device_control(hw_dev, HWTIMER_CTRL_MODE_SET, &mode);
//    if (ret != RT_EOK)
//    {
//        rt_kprintf("set mode failed! ret is :%d\n", ret);
//        return ret;
//    }
//
//    /* 设置定时器超时值为5s并启动定时器 */
//    timeout_s.sec = 5; /* 秒 */
//    timeout_s.usec = 0; /* 微秒 */
//
//    if (rt_device_write(hw_dev, 0, &timeout_s, sizeof(timeout_s)) != sizeof(timeout_s))
//    {
//        rt_kprintf("set timeout value failed\n");
//        return RT_ERROR;
//    }
//
////    /* 延时3500ms */
////    rt_thread_mdelay(1000);
////
////    /* 读取定时器当前值 */
////    rt_device_read(hw_dev, 0, &timeout_s, sizeof(timeout_s));
////    rt_kprintf("Read: Sec = %d, Usec = %d\n", timeout_s.sec, timeout_s.usec);
//
//    return ret;
//}
///* 导出到 msh 命令列表中 */
//MSH_CMD_EXPORT(hwtimer_sample, hwtimer sample);
//int init_app(void)
//{
//    uint8_t pin = rt_pin_get("PE.1");
//    rt_pin_mode(pin, PIN_MODE_OUTPUT);
//    GPIOECLR |= BIT(1);
//    while (1)
//    {
//        toggle_pin();
//        rt_thread_mdelay(1000);
//    }
//    return 0;
//
//}
//void toggle_pin(void)
//{
//    if (irrxpin_isHigh)
//    {
//        GPIOECLR |= BIT(1);
//        irrxpin_isHigh = 0;
//    }
//    else
//    {
//        GPIOESET |= BIT(1);
//        irrxpin_isHigh = 1;
//    }
//}

